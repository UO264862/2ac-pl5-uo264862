#include <time.h>

int main(){
	struct timespec tStart, tEnd;
	double dElapsedTimeS;

	// Start time measurement
	clock_gettime(CLOCK_REALTIME, &tStart);

	// Code to be measured
	// [...]
	// [...]

	// Finish time measurement
	clock_gettime(CLOCK_REALTIME, &tEnd);

	// Compute the elapsed time, in seconds
	dElapsedTimeS = (tEnd.tv_sec - tStart.tv_sec);
	dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9:

	return 0;
}
