#include <stdio.h>

int main()
{
	int i = 3;
	float f = 2.4;
	char * s = "Content";

	// Print the content of i
	printf("Content of i: %d\n", i);
	
	// Print the content of f
	printf("Content of f: %f\n", f);

	// Print the content of the string
	printf("The string says: '%s'\n", s);

	return 0;
}
