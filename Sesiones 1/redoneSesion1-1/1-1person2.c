#include <stdio.h>
#include <string.h>

struct _Person
{
	char name[30];
	int heightcm;
	double weightkg;
};
// This abbreviates the type name
typedef struct _Person Person;
typedef Person* PPerson;

int main(int argc, char* argv[])
{
	Person Peter;
	strcpy(Peter.name, "Peter");
	Peter.heightcm = 175;

	Person Javier;
	PPerson pJavier;

	// Assign the weight
	Peter.weightkg = 78.7;

	// Show the information of the Peter data structure on the screen
	printf("Peter's height: %d cm; Peter's weight: %f kg\n", Peter.heightcm, Peter.weightkg);

	// Memory location of Javier variable is assigned to the pointer
	pJavier = &Javier;
	Javier.heightcm = 180;
	Javier.weightkg = 84.0;
	pJavier->weightkg = 83.2;
	// pJavier->weightkg == (*pJavier) weightkg

	printf("Javier's height: %d cm; Javier's weight: %f kg\n", Javier.heightcm, Javier.weightkg
);

	return 0;
}
