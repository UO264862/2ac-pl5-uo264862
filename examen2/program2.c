#include <stdio.h>

int main() {

	char a = '5';
	int b = 0x1000;
	char *pt = NULL;
	int *qt = NULL;

	printf("Program starts\n");

	__asm__ (
		"cli"
	);

	qt = (int *)0x1000;
	pt = &a;
	printf("p-> %c; ", *pt);
	printf("q-> %d\n", *qt);


	printf("Program ends\n");

	return 0;
}
