#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

#include "3-4print-pte.h"

int global_var = 0x12345678;
int variable; // Global variable

// Given a virtual address, this virtual address is displayed on
// the screen, as well as its associated Page Table Entry, physical address
// and the flags associated to the Memory Page assigned. On top of this
// information, the title passed as argument is also displayed.
void print_virtual_physical_pte(void *virtual_addr, char *title)
{
	unsigned int physical_addr;
	unsigned int pte;
	unsigned int flags_vp;

	printf("\n%s\n", title);
	
	int local = 0xABCDEF01;

	print_virtual_physical_pte(&local, "Local variable\n-----------------");


	// Retrieve the entry in the page table for the virtual address
	if (get_pte(virtual_addr, &pte))
	{
		perror("Linmem module error");
		return;
	}

	// Is there PTE?
	if (pte)
	{ // OK
		// Store the flags associated with the memory page
		flags_vp = pte & 0x00000FFF;

		// Calculate the memory physical address
		physical_addr = (pte & 0xFFFFF000) + ((unsigned int)virtual_addr & 0x00000FFF);
		printf("Virtual address: \t%.8Xh\n"
				"Page Table Entry:\t%.8Xh\n"
				"Physical Address:\t%.8Xh\n"
				"Flags Virtual Page:\t%.3Xh\n",
				(unsigned int)virtual_addr, pte, physical_addr, flags_vp);
	}
	else
	{
		fprintf(stderr, "Page %.5Xh does not have a page table entry\n",
			(unsigned int)virtual_addr >> 12);
	}
}


int main(void)
{
	int local_var = 0xABCDEF01;

	printf("\nProcess Identifier (PID): %d\n", getpid());
	
		void *address1;
	void *address2;

	// Memory Address 1 initialization
	address1 = (void *) 0xC1600000;

	// Memory Address 2 initialization
	address2 = (void *) 0xC1AF9000;

	print_virtual_physical_pte((void *)address1, "\nKernel Area Address 1\n"
												"------------------");
	print_virtual_physical_pte((void *)address2, "\nKernel Area Address 2\n"
												"------------------");

	print_virtual_physical_pte((void *)&variable, "\nUser Area Address 3\n"
												"------------------");

	
	printf("\n---- Press [ENTER] to continue");
	getchar();
	return 0;
}
