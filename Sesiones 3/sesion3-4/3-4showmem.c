#include <stdio.h>

#include "3-4print-pte.h"

int variable; // Global variable

int main(void)
{
	void *address1;
	void *address2;

	// Memory Address 1 initialization
	address1 = (void *) 0xC1600000;

	// Memory Address 2 initialization
	address2 = (void *) 0xC1AF9000;

	print_virtual_physical_pte((void *)address1, "\nKernel Area Address 1\n"
												"------------------");
	print_virtual_physical_pte((void *)address2, "\nKernel Area Address 2\n"
												"------------------");

	print_virtual_physical_pte((void *)&variable, "\nUser Area Address 3\n"
												"------------------");

	printf("\n---- Press [ENTER] to continue");
	getchar();

	return 0;
}
