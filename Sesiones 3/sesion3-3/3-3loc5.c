#define NROWS    128 // 2^7 rows
#define NCOLS    128 // 2^7 columns
#define NTIMES   (4096*10)   // Repeat 40960 times

// Matrix size 2^14 = 16 KiBytes
char matrix[NROWS][NCOLS];

int main(void)
{
    int i, j, rep;

    for (rep = 0; rep < NTIMES; rep++)
    {
        for (i = 0; i < NROWS; i++)
        {
            for(j = 0; j < NCOLS; j++)
            {
                matrix[i][j] = 'A';
            }
        }
    }
}